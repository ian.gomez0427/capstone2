const Cart = require("../models/Cart.js");
const Product = require("../models/Product.js");


module.exports.viewCart = (userId) => {
  return Cart.findOne({ userId: userId })
    .populate('items.productId') // Populate the product details
    .then(cart => {
      if (!cart || cart.items.length === 0) {
        return { message: 'Found No Products on your Cart' };
      }

      // Calculate subtotal for each item and total amount
      cart.items.forEach(item => {
        item.subtotal = item.quantity * item.productId.price;
      });

      const totalAmount = cart.items.reduce((total, item) => total + item.subtotal, 0);

      return {
        data: {
          cartItems: cart.items,
          totalAmount: totalAmount
        }
      };
    })
    .catch(error => {
      return { error: 'An error occurred while retrieving the cart.' };
    });
};


module.exports.addProductToCart = (userId, cartData) => {
  
  const { productId, quantity } = cartData;
  
  return Product.findById(productId)
    .then(product => {
      if (!product) {
        return { error: 'Product not found' };
      }

      const subtotal = product.price * quantity;

      return Cart.findOne({ userId })
        .then(cart => {
          if (!cart) {
            const newCart = new Cart({
              userId,
              items: [{ productId, quantity, subtotal }],
              totalAmount: subtotal
            });
            return newCart.save();
          }

          cart.items.push({ productId, quantity, subtotal });
          cart.totalAmount += subtotal;

          return cart.save();
        })
        .then(savedCart => {
          return { message: 'Product added to cart successfully', data: savedCart };
        });
    })
    .catch(error => {
      return { error: 'An sows error occurred while adding the product to the cart.' };
    });
};

module.exports.editCartItem = (userId, cartItemId, updatedQuantity) => {
  if (!cartItemId || !updatedQuantity || updatedQuantity <= 0) {
    return { error: 'Issue on the input data' };
  }

  return Cart.findOne({ userId })
    .populate('items.productId')
    .then(cart => {
      if (!cart) {
        return { error: 'No Products placed on your Cart' };
      }

      const cartItem = cart.items.find(item => item._id.equals(cartItemId));
      if (!cartItem) {
        return { error: 'Cart item not found' };
      }

      const productPrice = cartItem.productId.price;
      cart.totalAmount -= cartItem.subtotal;

      cartItem.quantity = updatedQuantity;
      cartItem.subtotal = productPrice * updatedQuantity;
      // console.log('New Subtotal:', cartItem.subtotal);
      cart.totalAmount += cartItem.subtotal;
      // console.log('New Total Amount:', cart.totalAmount);

      return cart.save()
        .then(savedCart => {
          return { message: 'Cart item updated successfully', data: savedCart };
        });
    })
    .catch(error => {
      // console.error('Error:', error);
      return { error: 'An error occurred while updating the cart item.' };
    });
};


module.exports.removeCartItem = (userId, productId) => {
  if (!productId) {
    return { error: 'Enter the Product ID you want to delete from your cart' };
  }

  return Cart.findOneAndUpdate(
    { userId },
    { $pull: { items: { productId } } },
    { new: true }
  )
    .then(updatedCart => {
      if (!updatedCart) {
        return { error: 'No Products placed on your Cart' };
      }
      return { message: 'Cart item removed successfully', data: updatedCart };
    })
    .catch(error => {
      return { error: 'An error occurred while removing the cart item.' };
    });
};


