const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const CartController = require('../controllers/CartController.js');

// View Cart
router.get('/view', auth.verify, (req, res) => {
  CartController.viewCart(req.user._id)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while retrieving the cart.' });
    });
});


// Add Product to Cart
router.post('/add', auth.verify, (req, res) => {
  CartController.addProductToCart(req.user._id, req.body)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while adding the product to the cart.' });
    });
});

// Edit Cart Item
router.put('/edit', auth.verify, (req, res) => {
  const userId = req.user._id;
  const { cartItemId, updatedQuantity } = req.body; // Use lowercase property names

  CartController.editCartItem(userId, cartItemId, updatedQuantity)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while updating the cart item.' });
    });
});

router.delete('/remove/:productId', auth.verify, (req, res) => {
  const userId = req.user._id;
  const productId = req.params.productId;

  CartController.removeCartItem(userId, productId)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while removing the cart item.' });
    });
});


module.exports = router;
